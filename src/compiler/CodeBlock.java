package compiler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import types.IntType;
import types.Itype;
import types.RefType;
import types.boolType;

public class CodeBlock {

	private static final String SP = "0";

	static boolean ref = false;
	static boolean assign = false;
	static String nFrame;
	static String nloc;
	static String typeF = "";

	static public class StackFrame {
		int number;
		int ndecls;
		StackFrame parentFrame;
		static int fnumber = 1;

		StackFrame(int ndecls, StackFrame parentFrame) {
			this.number = fnumber++;
			this.ndecls = ndecls;
			this.parentFrame = parentFrame;
		}

		void dump() throws FileNotFoundException {
			// open file
			PrintStream f = new PrintStream(new File("frame.j"));
			f.println(".source frame.j" + "\n" + ".interface public frame" + "\n" + ".super java/lang/Object");
			PrintStream nf = new PrintStream(new File("frame_" + number + ".j"));

			// write header
			nf.println(".source frame_" + number + ".j");
			nf.println(".class frame_" + number);
			nf.println(".super java/lang/Object");
			nf.println(".implements frame");
			nf.println("\n");

			// write ndecls field declarations

			int aux = 0;
			for (int i = 0; i < ndecls; i++) {
				if (parentFrame != null && aux == 0) {
					nf.println(".field public SL Lframe_" + parentFrame.number + ";");
					aux++;
				}
				if (ref) {
					nf.println(".field public loc_" + i + " L" + typeF + ";");
				} else
					nf.println(".field public loc_" + i + " I");
			}

			// write footer
			nf.println("\n");
			nf.println(".method public <init>()V");
			nf.println("aload_0");
			nf.println("invokespecial java/lang/Object/<init>()V");
			nf.println("return");
			nf.println(".end method");
		}

		public String getType() {
			return "frame_" + number;
		}

		public int getNumber() {
			return this.number;
		}

		public StackFrame getParent() {
			return parentFrame;
		}
	}

	ArrayList<String> code;
	ArrayList<StackFrame> frames;
	StackFrame currentFrame;
	private int labelcounter = 0;
	Itype type;

	public CodeBlock() {
		code = new ArrayList<String>(100);
		frames = new ArrayList<StackFrame>();
	}

	// ------------------------------ decl frames
	// --------------------------------------------
	public StackFrame createFrame(int ndecls) {
		StackFrame frame = new StackFrame(ndecls, currentFrame);
		currentFrame = frame;

		code.add("new frame_" + currentFrame.number);
		code.add("dup");
		code.add("invokespecial frame_" + currentFrame.number + "/<init>()V");

		frames.add(frame);
		return frame;
	}

	// --------------------------------------------------GET_TYPE------------------------------//
	public void getType(Itype type) {
		this.type = type;
	}

	// --------------------------------------------------- var write
	// files---------------
	public void dumpRefs(Itype type) throws FileNotFoundException {
		if (typeF.isEmpty())
			typeF = getJasminTypeName(type);

		@SuppressWarnings("resource")
		PrintStream out = new PrintStream(new FileOutputStream(getJasminTypeName(type) + ".j"));
		System.err.println(getJasminTypeName(type));
		out.println(".source " + getJasminTypeName(type) + ".j");
		out.println(".class " + getJasminTypeName(type));
		out.println(".super java/lang/Object");
		out.println("\n");

		out.println(".field public value " + "" + getFinalType(type));

		// write footer
		out.println("\n");
		out.println(".method public <init>()V");
		out.println("aload_0");
		out.println("invokespecial java/lang/Object/<init>()V");
		out.println("return");
		out.println(".end method");

	}

	private int check_(String text) {
		int counter = 0;
		int i = 0;
		while (i < text.length()) {
			if (text.charAt(i) == '_')
				counter++;
			i++;
		}
		return counter;
	}

	private String getFinalType(Itype type) {
		ref = true;
		String t = getJasminTypeName(type);
		if (t.contains("_Int") && check_(t) == 1 || t.contains("_bool") && check_(t) == 1)
			return "I";

		t = t.substring(4);

		return "L" + t + ";";

	}

	public void emit_init_class(String className) {
		code.add("invokespecial " + className + "/<init>()V");
		code.add("dup");
	}

	public StackFrame getFrame() {
		return currentFrame;
	}

	public void emit_newframe(String ref) {
		code.add("new " + ref);
	}

	public void pushFrame(StackFrame frame) {
		code.add("astore " + SP);
		currentFrame = frame;
	}

	public void emit_dup() {
		code.add("dup");
	}

	public void ifeq(String label) {
		code.add("ifeq " + label);
	}

	public void go_to(String label) {
		code.add("goto " + label);
	}

	public void emitlabel(String label) {
		code.add(label + ":");
	}

	public void popFrame() {
		if (currentFrame.parentFrame == null) {
			code.add("\n");
			if (assign)
				code.add("getfield " + nFrame + "/loc_" + nloc + " L" + typeF + ";");
			code.add("aconst_null");
			code.add("astore " + SP);

		} else {
			code.add("\n");
			code.add("aload " + SP);
			code.add("checkcast " + currentFrame.getType());
			code.add("getfield " + currentFrame.getType() + "/SL "
					+ this.reftypeToJasmin(currentFrame.parentFrame.getType()));
			code.add("astore " + SP);

		}
		currentFrame = currentFrame.parentFrame;
	}

	public void putField(int loc, String type) {
		if (ref) {
			code.add("putfield frame_" + currentFrame.number + "/loc_" + loc + " L" + typeF + ";");
		} else
			code.add("putfield frame_" + currentFrame.number + "/loc_" + loc + " " + getJasminType(type));
	}

	public void emit_putfield(String className, String fieldName, String fieldType) {
		code.add("putfield " + className + "/" + fieldName + " " + fieldType);
	}

	public void storeFrame(int nframe, int offset) {
		code.add("aload " + SP);
		code.add("putfield frame_" + nframe + "/SL Lframe_" + frames.get(nframe - 1).parentFrame.number + ";");
		code.add("dup");
	}

	public void aload() {
		code.add("\n");
		code.add("aload " + SP);
	}

	public void checkCast(int frameN) {
		code.add("checkcast frame_" + frameN);
	}

	public void checkCastAssign(String ref) {
		code.add("checkcast " + ref);
	}

	public boolean isRef() {
		return !typeF.isEmpty();
	}

	public void getfield_convert_I() {

		code.add("getfield " + typeF + "/value" + " I");
	}

	public void getfield_ref(String nframe, String loc) {

		if (ref) {
			code.add("getfield " + nframe + "/loc_" + loc + " L" + typeF + ";");
			nFrame = nframe;
			nloc = loc;
		} else if (assign)
			code.add("getfield " + nframe + "/loc_" + loc + " I \n");
		else
			code.add("getfield " + nframe + "/loc_" + loc + " I \n");
		assign = false;
	}

	public void getfieldAssign_flag() {
		assign = true;

	}

	public void emit_getfield(String typeb, String value, String typeE) {
		code.add("getfield " + typeb + "/" + value + " " + typeE + ";");
	}

	// ------------------------------------------------get
	// types--------------------------
	public String getJasminTypeName(Itype t) { // ele recebe o tipo vindo da
												// class e depois e cnverter par
												// ao tipo jasmin

		if (t instanceof RefType) {
			RefType rt = (RefType) t;
			return "ref_" + getJasminTypeName(rt.getType());
		} else if (t instanceof IntType) {
			return "Int";
		} else if (t instanceof boolType) {
			return "Int";
		}
		return "UNDEFINED"; // Error
	}

	public String getJasminType(String type) {

		if (type.equals("Int"))
			return "I";
		else
			return "L" + type + ";";
	}

	private String reftypeToJasmin(String type) {
		return "L" + type + ";";
	}

	public void emit_pop() {
		code.add("pop");
	}

	public void emit_push(int n) {
		code.add("sipush " + n);
	}

	public void emit_add() {
		code.add("iadd");
	}

	public void emit_mul() {
		code.add("imul");
	}

	public void emit_div() {
		code.add("idiv");
	}

	public void emit_sub() {
		code.add("isub");
	}

	public void emit_loeq() {

		String label1 = createLabel();
		String label2 = createLabel();
		code.add("if_icmple " + label1);
		code.add("sipush 0");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 1");
		code.add(label2 + ":");

	}

	public void emit_hieq() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("if_icmpge " + label1);
		code.add("sipush 0");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 1");
		code.add(label2 + ":");
	}

	public void emit_higher() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("if_icmpgt " + label1);
		code.add("sipush 0");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 1");
		code.add(label2 + ":");
	}

	public void emit_less() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("if_icmplt " + label1);
		code.add("sipush 0");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 1");
		code.add(label2 + ":");
	}

	public void emit_dif() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("if_icmpne " + label1);
		code.add("sipush 0");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 1");
		code.add(label2 + ":");
	}

	public void emit_equal() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("if_icmpeq " + label1);
		code.add("sipush 0");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 1");
		code.add(label2 + ":");
	}

	public void emit_or() {
		code.add("ior");
	}

	public void emit_and() {
		code.add("iand");
	}

	public void emit_true() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("ifne " + label1);
		code.add("sipush 1");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 0");
		code.add(label2 + ":");
	}

	public void emit_false() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("ifeq " + label1);
		code.add("sipush 1");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 0");
		code.add(label2 + ":");
	}

	void dumpHeaderConsole(PrintStream out) {
		System.out.println(".class public Demo");
		System.out.println(".super java/lang/Object");
		System.out.println("");
		System.out.println(";");
		System.out.println("; standard initializer");
		System.out.println(".method public <init>()V");
		System.out.println("   aload_0");
		System.out.println("   invokenonvirtual java/lang/Object/<init>()V");
		System.out.println("   return");
		System.out.println(".end method");
		System.out.println("");
		System.out.println(".method public static main([Ljava/lang/String;)V");
		System.out.println("       ; set limits used by this method");
		System.out.println("       .limit locals 10");
		System.out.println("       .limit stack 256");
		System.out.println("");
		System.out.println("       ; setup local variables:");
		System.out.println("");
		System.out.println("       ;    1 - the PrintStream object held in java.lang.System.out");
		System.out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
		System.out.println("");
		System.out.println("       ; place your bytecodes here");
		System.out.println("       ; START");
		System.out.println("");
	}

	void dumpFooterConsole(PrintStream out) {
		System.out.println("       ; END");
		System.out.println("");
		System.out.println("");
		System.out.println("       ; convert to String;");
		System.out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		System.out.println("       ; call println ");
		System.out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		System.out.println("");
		System.out.println("       return");
		System.out.println("");
		System.out.println(".end method");
	}

	void dumpCode() {
		for (String s : code)
			System.out.println("       " + s);
	}

	public String createLabel() {

		return "label" + labelcounter++;

	}

	void dumpHeader(PrintStream out) {
		out.println(".class public Demo");
		out.println(".super java/lang/Object");
		out.println("");
		out.println(";");
		out.println("; standard initializer");
		out.println(".method public <init>()V");
		out.println("   aload_0");
		out.println("   invokenonvirtual java/lang/Object/<init>()V");
		out.println("   return");
		out.println(".end method");
		out.println("");
		out.println(".method public static main([Ljava/lang/String;)V");
		out.println("       ; set limits used by this method");
		out.println("       .limit locals 10");
		out.println("       .limit stack 256");
		out.println("");
		out.println("       ; setup local variables:");
		out.println("");
		out.println("       ;    1 - the PrintStream object held in java.lang.out");
		out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
		out.println("");
		out.println("       ; place your bytecodes here");
		out.println("       ; START");
		out.println("");
	}

	void dumpFooter(PrintStream out) {
		out.println("       ; END");
		out.println("");
		out.println("");
		out.println("       ; convert to String;");
		out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		out.println("       ; call println ");
		out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		out.println("");
		out.println("       return");
		out.println("");
		out.println(".end method");
	}

	void dumpCode(PrintStream out) {
		for (String s : code)
			out.println("       " + s);

	}

	void dumpCodeConsole(PrintStream out) {
		for (String s : code)
			System.out.println("       " + s);

	}

	private void dumpFrames() throws FileNotFoundException {
		for (StackFrame f : frames)
			f.dump();
	}

	public void dump(String filename) throws FileNotFoundException {

		PrintStream out = new PrintStream(new FileOutputStream(filename));

		dumpHeaderConsole(out);
		dumpCodeConsole(out);
		dumpFooterConsole(out);
		dumpHeader(out);
		dumpCode(out);
		dumpFooter(out);
		dumpFrames();

	}

}
