package compiler;

import java.util.ArrayList;

import util.DuplicateIdentifierException;
import util.Environment;
import util.UndeclaredIdentifierException;

public class CompilerEnv extends Environment<Integer> {

	static class Assoc<T> {
		String id;
		T value;

		Assoc(String id, T value) {
			this.id = id;
			this.value = value;
		}
	}

	public static class Address {
		int jumps;
		int offset;

		Address(int jumps, int offset) {
			this.jumps = jumps;
			this.offset = offset;
		}

		public int getJumps() {
			return jumps;
		}

		public int getOffset() {
			return offset;
		}
	}

	private CompilerEnv up;
	ArrayList<Assoc<Integer>> assocs;

	public CompilerEnv() {
		this.up = null;
		this.assocs = new ArrayList<Assoc<Integer>>();
	}

	private CompilerEnv(CompilerEnv up) {
		this();
		this.up = up;
	}

	public CompilerEnv beginScope() {

		return new CompilerEnv(this);
	}

	public CompilerEnv endScope() {
		return up;
	}

	public int addStackSlot(String id) throws DuplicateIdentifierException {
		int size = assocs.size();
		assocs.add(new Assoc<Integer>(id, size++));

		return size;
	}

	public Address lookup(String id) throws UndeclaredIdentifierException {
		int jumps = 1;
		System.out.println("idLookup: " + id);
		CompilerEnv current = this;

		while (current != null) {
			for (Assoc<Integer> assoc : current.assocs) {
				System.out.println("assoc: " + assoc + "jumps: " + jumps);
				if (assoc.id.equals(id)) {
					return new Address(jumps, assoc.value); // o value e o
															// offset
				}
			}
			current = current.up;
			jumps++;
		}
		throw new UndeclaredIdentifierException(id);
	}
	// public void assoc( String id, T value ) throws
	// DuplicateIdentifierException {
	//
	// for(Assoc<T> assoc: assocs)
	// if(assoc.id.equals(id))
	// throw new DuplicateIdentifierException(id);
	//
	// assocs.add(new Assoc<T>(id,value));
	//
	// }
}
