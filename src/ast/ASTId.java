package ast;

import compiler.CodeBlock;
import compiler.CodeBlock.StackFrame;
import compiler.CompilerEnv;
import compiler.CompilerEnv.Address;
import types.Itype;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.Ivalue;

public class ASTId implements ASTNode {

	String id;
	Itype type;

	public ASTId(String id) {
		this.id = id;
	}

	public Ivalue eval(Environment<Ivalue> env)
			throws UndeclaredIdentifierException, ExecutionErrorException, DynamicTypeError {
		return env.find(id);
	}

	@Override
	public String toString() {
		return id;
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws DuplicateIdentifierException,
			UndeclaredIdentifierException, ExecutionErrorException, TypeErrorException {

		Address p = env.lookup(id);

		StackFrame f = code.getFrame();
		code.aload();
		code.checkCast(f.getNumber());

		for (int i = 1; i < p.getJumps(); i++, f = f.getParent()) {
			System.out.println(p.getJumps());
			System.out.println("frame: " + f.getNumber());
			code.emit_getfield(f.getType(), "SL", "L" + f.getParent().getType());
		}
		code.getfield_ref(f.getType(), Integer.toString(p.getOffset()));

	}

	@Override
	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {
		type = env.find(id);
		return type;
	}

}

// public void compile(CodeBlock c, CompilerFrame compilerFrame) throws
// UndeclaredIdentifierException, ExecutionErrorException {
// c.emit_aload("1");
// c.emit_checkcast(compilerFrame.getName());
//
// CompilerFrame currente_frame = compilerFrame;
//
// while (currente_frame.hasAncestor() || currente_frame.containsId(id)) {
// if (currente_frame.containsId(id)) {
// c.emit_getfield(currente_frame.getName(), id,
// c.typeToJasmin(currente_frame.getTypeOfId(id)));
// return;
// }
// else {
// c.emit_getfieldSL(currente_frame.getName(),
// currente_frame.getAncestor().getName());
// currente_frame = currente_frame.getAncestor();
// }
// }
// throw new UndeclaredIdentifierException(id);
// }
//
// }

// c.emit_aload("1");
// c.emit_checkcast(compilerFrame.getName());
//
// CompilerEnv current_frame = compilerEnv;
//
// while (current_frame.hasAncestor() || current_frame.containsId(id)) {
// if (current_frame.containsId(id)) {
// c.emit_getfield(current_frame.getName(), id,
// c.typeToJasmin(current_frame.getTypeOfId(id)));
// return;
// }
// else {
// c.emit_getfieldSL(current_frame.getName(),
// current_frame.getAncestor().getName());
// current_frame = current_frame.getAncestor();
// }
