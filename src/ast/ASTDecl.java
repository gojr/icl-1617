package ast;

import java.util.ArrayList;

import compiler.CodeBlock;
import compiler.CodeBlock.StackFrame;
import types.Itype;
import compiler.CompilerEnv;
import util.Binding;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.Ivalue;

public class ASTDecl implements ASTNode {

	ArrayList<Binding> decls;
	ASTNode expr;
	Itype type;

	public ASTDecl(ArrayList<Binding> decls, ASTNode expr) {
		this.decls = decls;
		this.expr = expr;
	}

	@Override
	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {
		Ivalue value;

		Environment<Ivalue> newEnv = env.beginScope();
		for (Binding decl : decls) {
			value = decl.getExpr().eval(env);
			newEnv.assoc(decl.getId(), value);
		}
		value = expr.eval(newEnv);
		newEnv.endScope();

		return value;
	}

	@Override
	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {
		Itype idtype;
		Environment<Itype> envt = env.beginScope();

		for (Binding decl : decls) {
			idtype = decl.getExpr().typeCheck(env);
			envt.assoc(decl.getId(), idtype);
		}
		type = expr.typeCheck(envt);
		envt.endScope();

		return type;
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws DuplicateIdentifierException,
			UndeclaredIdentifierException, ExecutionErrorException, TypeErrorException {

		CompilerEnv newEnv;
		StackFrame frame = code.createFrame(decls.size()); // o decls.size
															// possui o numero
															// de variaveis
															// dentro de cada
															// decl

		newEnv = env.beginScope();
		int loc = 0;

		for (Binding decl : decls) {
			if (frame.getNumber() == 1) {
				code.emit_dup();
				decl.getExpr().compile(code, env);// o get expr vai buscar o
													// valor das variaveis
				code.putField(loc++, code.getJasminTypeName(type));
				newEnv.addStackSlot(decl.getId());
			} else {
				code.emit_dup();
				int offset = newEnv.addStackSlot(decl.getId());
				code.storeFrame(frame.getNumber(), offset);
				decl.getExpr().compile(code, env);
				code.putField(loc++, code.getJasminTypeName(type));
			}

		}
		code.pushFrame(frame);
		expr.compile(code, newEnv);// compila a expressao;
		newEnv = env.endScope();
		code.popFrame();

	}

	public String toString() {
		String s = "";
		for (Binding decl : decls)
			s += decl.getId() + " = " + decl.getExpr().toString();
		return "decl " + s + " in " + expr.toString() + " end";
	}

}
