package ast;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.IntType;
import types.Itype;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.IntValue;
import values.Ivalue;

public class ASTNum implements ASTNode {

	int val;

	public Ivalue eval(Environment<Ivalue> env) throws DynamicTypeError {
		return new IntValue(val);
	}

	public ASTNum(int n) {
		this.val = n;
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env)
			throws DuplicateIdentifierException, ExecutionErrorException, TypeErrorException {
		code.emit_push(val);
	}

	@Override
	public String toString() {
		return Integer.toString(val);
	}

	@Override
	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {
		return new IntType(val);
	}
}
