package ast;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.IntType;
import types.Itype;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.IntValue;
import values.Ivalue;

public class ASTMul implements ASTNode {

	ASTNode left, right;

	public ASTMul(ASTNode l, ASTNode r) {
		this.left = l;
		this.right = r;
	}

	@Override
	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {
		Ivalue l = left.eval(env);
		Ivalue r = right.eval(env);
		if (l instanceof IntValue && r instanceof IntValue)
			return new IntValue(((IntValue) l).getValue() * ((IntValue) r).getValue());
		else
			throw new DynamicTypeError("EXPECTING INTEGER VALUE");
	}

	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {
		Itype t1 = left.typeCheck(env);
		Itype t2 = right.typeCheck(env);

		if (t1 instanceof IntType && t2 instanceof IntType) {
			return IntType.TYPE;
		} else
			throw new TypeErrorException("EXPECTED INTEGER TYPES");
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws DuplicateIdentifierException,
			UndeclaredIdentifierException, ExecutionErrorException, TypeErrorException {
		this.left.compile(code, env);
		this.right.compile(code, env);
		code.emit_mul();
	}

	@Override
	public String toString() {
		return left.toString() + " * " + right.toString();
	}
}
