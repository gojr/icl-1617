package ast;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.Itype;
import types.boolType;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.Ivalue;

public class ASTBoolean implements ASTNode {

	boolean val;

	public ASTBoolean(boolean value) {
		val = value;

	}

	@Override
	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {
		return new BoolValue(val);
	}

	@Override
	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {
		return new boolType(val);
	}

	@Override
	public String toString() {
		return Boolean.toString(val);
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env)
			throws DuplicateIdentifierException, ExecutionErrorException, TypeErrorException {
		if (val)
			code.emit_push(1);
		else
			code.emit_push(0);
	}

}
