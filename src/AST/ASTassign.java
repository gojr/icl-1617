package ast;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.Itype;
import types.RefType;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.Ivalue;
import values.RefValue;

public class ASTassign implements ASTNode {

	ASTNode left, right;
	RefType type;

	public ASTassign(ASTNode l, ASTNode r) {
		this.left = l;
		this.right = r;
	}

	@Override
	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {
		RefValue ref = (RefValue) left.eval(env);
		Ivalue value = right.eval(env);
		if (ref instanceof RefValue) {
			ref.setValue(value);
			return value;
		} else
			throw new DynamicTypeError("ERRO");

	}

	@Override
	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {
		RefType t1 = (RefType) left.typeCheck(env);
		Itype t2 = right.typeCheck(env);

		String leftType = t1.getType().toString();
		String rightType = t2.toString();

		if (leftType.equals(rightType)) {
			type = t1;
			return type;
		}

		else
			throw new TypeErrorException("EXPECTED SAME TYPE");
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws DuplicateIdentifierException, ExecutionErrorException,
			TypeErrorException, UndeclaredIdentifierException {

		String className = code.getJasminTypeName(type);
		left.compile(code, env);
		right.compile(code, env);
		code.emit_putfield(className, "value", code.getJasminType(code.getJasminTypeName(type.getType())));
		code.emit_dup();
		code.aload();
		code.getfieldAssign_flag();

	}

	public String toString() {
		return left.toString() + ":=" + right.toString();
	}

}
