package ast;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.Itype;
import types.RefType;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.Ivalue;
import values.RefValue;

public class ASTDeref implements ASTNode {

	ASTNode exp;
	Itype type;

	public ASTDeref(ASTNode exp) {
		this.exp = exp;
	}

	@Override
	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {
		Ivalue exp = this.exp.eval(env);

		if (exp instanceof RefValue)
			return ((RefValue) exp).getValue();
		else
			throw new DynamicTypeError("EXPECTED REF TYPE");
	}

	@Override
	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {
		type = exp.typeCheck(env);

		if (type instanceof RefType) {
			type = ((RefType) type).getType();
			return type;
		}

		else
			throw new TypeErrorException("EXPECTED REF TYPE");
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws DuplicateIdentifierException, ExecutionErrorException,
			TypeErrorException, UndeclaredIdentifierException {
		exp.compile(code, env);
		boolean var = code.isRef();
		if (var) {
			code.getfield_convert_I();
		}
	}

	@Override
	public String toString() {
		return "!" + exp;
	}
}
