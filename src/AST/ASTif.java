package ast;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.Itype;
import types.boolType;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.Ivalue;

public class ASTif implements ASTNode {

	ASTNode cond, thenif, elseif;
	Itype type;

	public ASTif(ASTNode cond, ASTNode thenif, ASTNode elseif) {
		this.cond = cond;
		this.thenif = thenif;
		this.elseif = elseif;
	}

	@Override
	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {
		BoolValue c = (BoolValue) this.cond.eval(env);
		if (c.getValue()) {
			return thenif.eval(env);
		} else
			return elseif.eval(env);

	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws DuplicateIdentifierException, ExecutionErrorException,
			TypeErrorException, UndeclaredIdentifierException {
		String con = code.createLabel();
		String exit = code.createLabel();

		cond.compile(code, env);
		code.ifeq(con);
		thenif.compile(code, env);
		code.go_to(exit);
		code.emitlabel(con);
		elseif.compile(code, env);
		code.emitlabel(exit);

	}

	@Override
	public String toString() {
		return "if " + cond.toString() + "?" + thenif.toString() + " :" + elseif.toString();
	}

	@Override
	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {

		Itype astcond = cond.typeCheck(env);
		System.out.println(astcond.toString());
		if (!(astcond instanceof boolType)) {

			throw new TypeErrorException("EXPECTING BOOLEAN TYPE");
		}
		Itype ifExp = this.thenif.typeCheck(env);
		Itype elseExp = this.elseif.typeCheck(env);

		return ifExp;
	}

}
