package ast;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.IntType;
import types.Itype;
import types.boolType;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IntValue;
import values.Ivalue;

public class ASTDif implements ASTNode {

	ASTNode left, right;

	public ASTDif(ASTNode l, ASTNode r) {
		this.left = l;
		this.right = r;
	}

	@Override
	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {

		Ivalue l = left.eval(env);
		Ivalue r = right.eval(env);

		if (l instanceof BoolValue && r instanceof BoolValue) {
			return new BoolValue(((BoolValue) l).getValue() != ((BoolValue) r).getValue());
		} else if (l instanceof IntValue && r instanceof IntValue) {
			return new BoolValue(((IntValue) l).getValue() != ((IntValue) r).getValue());

		} else
			throw new DynamicTypeError("DIFERENT TYPE VALUES");
	}

	@Override
	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {
		Itype t1 = left.typeCheck(env);
		Itype t2 = right.typeCheck(env);

		if (t1 instanceof IntType && t2 instanceof IntType) {
			return IntType.TYPE;
		}

		if (t1 instanceof boolType && t2 instanceof boolType) {
			return boolType.TYPE;
		}

		else
			throw new TypeErrorException("EXPECTED SAME TYPES");
	}

	@Override
	public String toString() {
		return left.toString() + "!=" + right.toString();
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws DuplicateIdentifierException,
			UndeclaredIdentifierException, ExecutionErrorException, TypeErrorException {
		left.compile(code, null);
		right.compile(code, null);
		code.emit_dif();
	}
}
