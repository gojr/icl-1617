package ast;

import java.io.FileNotFoundException;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.Itype;
import types.RefType;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.Ivalue;
import values.RefValue;

public class ASTVar implements ASTNode {

	ASTNode var;
	RefType type;

	public ASTVar(ASTNode var) {
		this.var = var;

	}

	@Override
	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {

		return new RefValue(var.eval(env));
	}

	@Override
	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {
		Itype t = var.typeCheck(env);
		type = new RefType(t);
		return type;
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws DuplicateIdentifierException,
			UndeclaredIdentifierException, ExecutionErrorException, TypeErrorException {

		try {
			String className = code.getJasminTypeName(type);
			code.emit_newframe(className);
			code.emit_dup();
			code.emit_init_class(className);
			var.compile(code, env);
			code.emit_putfield(className, "value", code.getJasminType(code.getJasminTypeName(type.getType())));
			code.dumpRefs(type);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	@Override
	public String toString() {
		return "var(" + var + ")";
	}

}
