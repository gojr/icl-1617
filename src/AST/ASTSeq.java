package ast;

import java.util.ArrayList;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.Itype;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.Ivalue;

public class ASTSeq implements ASTNode {
	ArrayList<ASTNode> seq = new ArrayList<ASTNode>();

	public ASTSeq(ArrayList<ASTNode> seq) {
		this.seq = seq;
	}

	@Override
	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {
		int i;
		for (i = 0; i < seq.size() - 1; i++)
			seq.get(i).eval(env);

		return seq.get(i).eval(env);
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws DuplicateIdentifierException,
			UndeclaredIdentifierException, ExecutionErrorException, TypeErrorException {
		int i;
		for (i = 0; i < seq.size() - 1; i++) {
			seq.get(i).compile(code, env);

			code.emit_pop();
		}
		seq.get(i).compile(code, env);

	}

	@Override
	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {

		for (ASTNode n : seq) {
			n.typeCheck(env);
		}
		return seq.get(seq.size() - 1).typeCheck(env);
	}

	public String toString() {
		String parsed = "";
		for (ASTNode aux : seq) {
			parsed += aux.toString() + ";";
		}
		parsed = parsed.substring(0, parsed.length() - 1);
		return parsed;
	}

}
