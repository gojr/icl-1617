package ast;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.IntType;
import types.Itype;
import types.boolType;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IntValue;
import values.Ivalue;

public class ASTHieq implements ASTNode {

	ASTNode left, right;

	public ASTHieq(ASTNode l, ASTNode r) {
		this.left = l;
		this.right = r;
	}

	@Override
	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {
		Ivalue l = left.eval(env);
		Ivalue r = right.eval(env);

		if (l instanceof IntValue && r instanceof IntValue) {
			return new BoolValue(((IntValue) l).getValue() >= ((IntValue) r).getValue());
		} else
			throw new DynamicTypeError("EXPECTING INTEGER VALUE");

	}

	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {
		Itype t1 = left.typeCheck(env);
		Itype t2 = right.typeCheck(env);

		if (t1 instanceof IntType && t2 instanceof IntType) {
			return boolType.TYPE;
		} else
			throw new TypeErrorException("EXPECTED INTEGER TYPES");
	}

	@Override
	public String toString() {
		return left.toString() + ">=" + right.toString();
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws DuplicateIdentifierException,
			UndeclaredIdentifierException, ExecutionErrorException, TypeErrorException {
		left.compile(code, env);
		right.compile(code, env);
		code.emit_hieq();
	}

}
