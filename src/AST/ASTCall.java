package ast;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.FuncType;
import types.Itype;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTCall implements ASTNode {

	ASTNode left, right;

	public ASTCall(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {
		ClosureValue closure = (ClosureValue) left.eval(env);
		Ivalue argument = right.eval(env);

		Environment<Ivalue> newenv = closure.getEnv();
		newenv.assoc(closure.getId(), argument);
		return closure.getExp().eval(newenv);

	}

	@Override
	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {
		Itype leftType = left.typeCheck(env);
		Itype rightType = right.typeCheck(env);

		if (leftType instanceof FuncType) {
			FuncType funcType = (FuncType) leftType;
			if (funcType.getIniType().toString() == rightType.toString()) {
				return funcType.getRetType();
			} else {
				throw new TypeErrorException("DIFERENTS TYPES");
			}
		} else {
			throw new TypeErrorException("FUNCTION TYPE ERROR");
		}
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv cenv) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		return left.toString() + " (" + right.toString() + ")";
	}

}