package ast;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.IntType;
import types.Itype;
import types.boolType;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IntValue;
import values.Ivalue;

public class ASTWhile implements ASTNode {

	ASTNode cond, body;

	public ASTWhile(ASTNode cond, ASTNode body) {
		this.cond = cond;
		this.body = body;
	}

	@Override
	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {
		BoolValue c = (BoolValue) cond.eval(env);

		while (c.getValue()) {
			body.eval(env);
			c = (BoolValue) cond.eval(env);
		}

		return c;
	}

	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {

		Itype condType = cond.typeCheck(env);

		if (condType != boolType.TYPE)
			throw new TypeErrorException("EXPECTING EXPRESSION TYPE INTEGER");

		body.typeCheck(env);
		return boolType.TYPE;
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws DuplicateIdentifierException, ExecutionErrorException,
			TypeErrorException, UndeclaredIdentifierException {
		String condition = code.createLabel();
		String exit = code.createLabel();

		code.emitlabel(condition);
		cond.compile(code, env);
		code.ifeq(exit);
		body.compile(code, env);
		code.emit_pop();
		code.go_to(condition);
		code.emitlabel(exit);
		code.emit_push(0);

	}

	public String toString() {
		return "while " + cond + " { " + body + " } ";
	}

}
