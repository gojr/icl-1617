package ast;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.Itype;
import types.boolType;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.Ivalue;

public class ASTBooleanNot implements ASTNode {

	ASTNode val;
	private int value;

	public ASTBooleanNot(ASTNode value) {
		val = value;

	}

	@Override
	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {
		if (val instanceof BoolValue)
			return new BoolValue(!((BoolValue) val.eval(env)).getValue());
		else
			throw new DynamicTypeError("EXPECTING Boolean or expression");

	}

	@Override
	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {
		if (val instanceof boolType)
			return boolType.TYPE;
		else
			throw new TypeErrorException("EXPECTING BOOLEAN");
	}

	@Override
	public String toString() {
		return val.toString();
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env)
			throws DuplicateIdentifierException, ExecutionErrorException, TypeErrorException {

		if (value == 1)
			code.emit_true();
		else if (value == 0)
			code.emit_false();
	}

}
