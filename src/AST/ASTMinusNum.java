package ast;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.IntType;
import types.Itype;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.Ivalue;
import values.IntValue;

public class ASTMinusNum implements ASTNode {

	ASTNode val;

	public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
			ExecutionErrorException, DynamicTypeError {
		return new IntValue(-((IntValue) val.eval(env)).getValue());

	}

	public Itype typeCheck(Environment<Itype> env) throws DuplicateIdentifierException, UndeclaredIdentifierException,
			ExecutionErrorException, TypeErrorException {
		return IntType.TYPE;
	}

	public ASTMinusNum(ASTNode n) {
		val = n;
	}

	@Override
	public String toString() {
		return "-" + val.toString();
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env)
			throws DuplicateIdentifierException, ExecutionErrorException, TypeErrorException {

		code.emit_push(-Integer.parseInt(val.toString()));
	}
}
