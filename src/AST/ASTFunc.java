package ast;

import compiler.CodeBlock;
import compiler.CompilerEnv;
import types.FuncType;
import types.Itype;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.ClosureValue;
import values.Ivalue;

public class ASTFunc implements ASTNode {
	 
	private String id;
	private ASTNode exp;
	Itype type;

		public ASTFunc(String id, ASTNode exp,Itype type) {
			this.id = id;
			this.exp = exp;
			this.type=type;
		}

		@Override
		public Ivalue eval(Environment<Ivalue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
		ExecutionErrorException, DynamicTypeError {
			return new ClosureValue(id, exp, env);
		}
		

		public Itype typeCheck(Environment<Itype> env) throws TypeErrorException, DuplicateIdentifierException, UndeclaredIdentifierException,
		ExecutionErrorException {

				Environment<Itype> envt = env.beginScope();
				envt.assoc(id, type);
				Itype returnType = exp.typeCheck(envt);
				return new FuncType(type, returnType); 
			}

		@Override
		public void compile(CodeBlock c, CompilerEnv env) throws UndeclaredIdentifierException, DuplicateIdentifierException,
		ExecutionErrorException {
			//TODO por fazer
		}

		public String toString(){
			return "fun" + id + " => " + exp.toString() + " end";
		}

}
