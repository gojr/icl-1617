package main;

import compiler.*;
import parser.ParseException;
import parser.Parser;
import types.Itype;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import ast.ASTNode;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;

public class Compiler {

	public static void main(String args[]) {
		Parser parser = new Parser(System.in);
		ASTNode exp;
		Itype type;

		try {
			exp = parser.Start();
			CompilerEnv env = new CompilerEnv();
			CodeBlock code = new CodeBlock();
			type = exp.typeCheck(new Environment<Itype>());
			exp.compile(code, env);
			code.getType(type);
			code.dump("Demo.j");

		} catch (Exception e) {
			System.out.println("Syntax Error!" + e); 
			e.printStackTrace();
		}
	}

	public static void compile(String s) throws ParseException, FileNotFoundException, DuplicateIdentifierException,
			UndeclaredIdentifierException, ExecutionErrorException, TypeErrorException {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		ASTNode n = parser.Start();
		CodeBlock code = new CodeBlock();
		CompilerEnv env = new CompilerEnv();
		n.compile(code, env);

		code.dump("Demo.j");
	}

}
