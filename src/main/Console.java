package main;

import java.io.ByteArrayInputStream;

import ast.ASTNode;
import parser.ParseException;
import parser.Parser;
import types.Itype;
import util.DuplicateIdentifierException;
import util.DynamicTypeError;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.Ivalue;



public class Console {

	@SuppressWarnings("static-access")
	public static void main(String args[]) throws DynamicTypeError, TypeErrorException {
		Parser parser = new Parser(System.in);

		while (true) {
			try {
				ASTNode n = parser.Start();
				Environment<Ivalue> env = new Environment<>();
				Environment<Itype> envt = new Environment<>();
				System.out.println("OK! - " + n.toString() + " = " +n.eval(env));
				System.out.println("OK! -" + n.typeCheck(envt));
			} catch (ParseException e) {
				System.out.println("Syntax Error!");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (UndeclaredIdentifierException e) {
				System.out.println("Undeclared identifier " + e.getId() +"!");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (DuplicateIdentifierException e) {
				System.out.println("Duplicated identifier " + e.getId() +"!");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (ExecutionErrorException e) {
				System.out.println("Internal Error");
				e.printStackTrace();
				parser.ReInit(System.in);
			}
		}
	}

	

	public static boolean accept(String s) throws ParseException {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			parser.Start();
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public static boolean acceptCompare(String s, Ivalue value) {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			ASTNode n = parser.Start();
			Environment<Ivalue> env = new Environment<>();
			return n.eval(env).toString().equals(value.toString());
		} catch (Exception e) {
			return false;
		}
	}

}
