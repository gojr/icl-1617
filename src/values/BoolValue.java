package values;

public class BoolValue implements Ivalue {
	boolean value;

	public BoolValue(boolean value) {
		this.value = value;
	}

	public boolean getValue() {
		return this.value;
	}

	public String toString() {
		return Boolean.toString(value);
	}

}
