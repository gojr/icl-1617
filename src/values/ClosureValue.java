package values;

import ast.ASTNode;
import util.Environment;

public class ClosureValue implements Ivalue {

	Environment<Ivalue> env;
	String id;
	ASTNode exp;

	public ClosureValue(String id, ASTNode exp, Environment<Ivalue> env) {
		this.id = id;
		this.exp = exp;
		this.env = env;
	}

	public String getId() {
		return this.id;
	}

	public ASTNode getExp() {
		return this.exp;
	}

	public Environment<Ivalue> getEnv() {
		return this.env;
	}

	public void setEnv(Environment<Ivalue> env) {
		this.env = env;
	}

}
