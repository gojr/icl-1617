package values;

public class IntValue implements Ivalue {

	int value;

	public IntValue(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}

	public String toString() {
		return Integer.toString(value);
	}
}
