package values;

public class RefValue implements Ivalue {
	Ivalue value;

	public RefValue(Ivalue value) {
		this.value = value;
	}

	public Ivalue getValue() {
		return this.value;
	}

	public void setValue(Ivalue value) {
		this.value = value;
	}

	public String toString() {
		return value.toString();
	}

}
