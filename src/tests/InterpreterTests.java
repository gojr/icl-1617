package tests;

import static org.junit.Assert.*;
import org.junit.Test;
import main.Console;
import parser.ParseException;
import values.Ivalue;
import values.BoolValue;
import values.IntValue;

public class InterpreterTests {
	
	
	private void testCase(String expression, Ivalue value) throws ParseException {
		assertTrue(Console.acceptCompare(expression,value));		
	}
	
	
	private void testNegativeCase(String expression, Ivalue value) throws ParseException {
		assertFalse(Console.acceptCompare(expression,value));
	}
	
	@Test
	public void test01() throws Exception {
		testCase("1\n", new IntValue(1));
	}
	
	@Test
	public void testBooleanOperations() throws Exception {
		testCase("1==1\n", new BoolValue(true));
		testCase("1==2\n", new BoolValue(false));
		testCase("1!=1\n", new BoolValue(false));
		testCase("1!=2\n", new BoolValue(true));
		testCase("2>1\n", new BoolValue(true));
		testCase("2>3\n", new BoolValue(false));
		testCase("2<3\n", new BoolValue(true));
		testCase("4<3\n", new BoolValue(false));
		testCase("1>-1\n", new BoolValue(true));
	}
	
	@Test
	public void testArithmeticOperations() throws Exception {
		testCase("1+2\n",new IntValue(3));
		testCase("1-2-3\n",new IntValue(-4));
		testCase("4*2\n",new IntValue(8));
		testCase("4/2/2\n",new IntValue(1));
		testCase("3+(2*3)\n",new IntValue(9));
		testCase("2+2*(4+1)\n",new IntValue(12));
		testCase("5*5/2\n",new IntValue(12));
		
	}
	
	@Test
	public void testDeclVarsWhilesIfsAssigns() throws Exception {
		testCase("decl x=1 in x end\n",new IntValue(1));
		testCase("decl x = decl y = 3 in 10*y end in x + decl y = 2 in x+y end end\n",new IntValue(62));
		testCase("decl x = 1 in decl x = 2 in x*5 end + x end\n", new IntValue(11));
		testCase("decl x = 1 y = 2 in decl x = 3 z = 4 in x + y + z end end\n",new IntValue(9));
		testCase("decl x = var(1) in x := 2; *x + 1 end\n", new IntValue(3));
		testCase("decl x = var(1) in *x end\n", new IntValue(1));
		testCase("decl x = var(1) in *x + 3 end\n",new IntValue(4));
		testCase("decl x = var(1)  in while *x < 10 { x := *x + 1 } end ; *x end\n", new IntValue(10));
		testCase("decl x = var(1)  in while *x < 10 { x := *x + 1 } end ; *x - 5 end\n", new IntValue(5));
		testCase("decl x = var(1)  in while *x < 10 { x := *x + 1 } end ; *x - 5 ; *x end\n", new IntValue(10));
		testCase("decl x = var(1)  in if *x == 1 ? while *x < 10 { x := *x + 1 } end : 10 end ; *x end\n", new IntValue(10));
		testCase("decl x = var(1)  in if *x == 1 ? true : while *x < 10 { x := *x + 1 } end end ; *x end\n", new IntValue(1));
		testCase("decl x=var(1) in decl y=x in decl w=y in w:= *w+*y;*x end end end\n", new IntValue(2));
		//testCase("decl x = var(1) in decl y = var(x) in if *x>10 ? 0 : x:=**y+1; x:=*x+1 end end end\n", new IntValue(3));
		testCase("decl x=var(true) in x:= *x && false end\n", new BoolValue(false));
		testCase("decl x=var(1) in decl y=x in x:= 3; *y end end\n", new IntValue(3));
		testCase("decl x=var(true) in x:= *x || false end\n", new BoolValue(true));
		testCase("decl x=var(true) in *x end\n", new BoolValue(true));
		testCase("decl x = var(0) in decl y = x := *x + 1 in 2 * *x end end\n", new IntValue(2));
	}
	
	@Test
	public void testIf() throws Exception{
		testCase("if false ? 1 : 2 end\n",new IntValue(2));
		testCase("if true ? 1 : 2 end\n",new IntValue(1));
		testCase("if true || false ? 1 : 2 end\n",new IntValue(1));
		testCase("decl x=var(true) y=var(5) in if *x && false ? 1 : y:= *y + 2 end end\n",new IntValue(7));
		testCase("decl x=var(true) y=var(5) in if *x && false ? 1 : decl w=var(4) in w:= *y + *w end end end\n",new IntValue(9));
		testCase("decl x=var(true) y=var(5) in if false ? 1 : if *x ? y:= *y + 3 : 0 end end end\n",new IntValue(8));
		testCase("decl x=1 in if x==1 ? 10 : 20 end end\n",new IntValue(10));
		testCase("decl x=1 in decl y=2 in if x>y ? x : y end end end\n",new IntValue(2));
		
	}
	
	@Test
	public void testWhile() throws Exception{
		testCase("decl x=var(0) in while *x<2 { x:=*x+1 } end end\n",new BoolValue(false));
		testCase("decl x=var(1) y=var(1) in while *x < 3 { y:= *x + *y; x:= *x + 1} end end\n",new BoolValue(false));		
	}
	
	
	@Test
	public void testSequence() throws Exception{
		
		testCase("1;2;3\n",new IntValue(3));
		testCase("decl x=var(1) in if false ? 0 : x:=*x+1; *x+1 end end\n",new IntValue(3));
		testCase("1;2;3;false\n",new BoolValue(false));
		testCase("decl x=var(1) in if x:=*x+1; *x>10 ? 0 : x:=*x+1; *x+1 end end\n",new IntValue(4));
		testCase("decl x=var(1) y=var(5) in while *x < 5 { y:= *x * *y; x:= *x + 1 } end ; *y end\n",new IntValue(120));
		testCase("decl x=var(true) y=var(5) in while *x { x:= false} end; if *x ? 1 : *y end end\n",new IntValue(5));
		testCase("decl x=var(true) y=var(5) in while *y < 7 { y:= *y + 1 } end; if *x && *y==7 ? 1 : 0 end end\n",new IntValue(1));
		testCase("decl x=var(true) y=var(5) in while *y < 7 { y:= *y + 1 } end; if *x && *y==7 ? 1;false : true end end\n",new BoolValue(false));
		//testCase("decl x=1 in while x!=3 { x++ } end ; 2 end\n",new IntValue(2));			
		
	}

	@Test
	public void testFun() throws Exception{ // ao correr a mao funciona
		testCase("fun x:int => x+1 end (2) \n",new IntValue(3));
		testCase("fun x:int => x*3+4 end (4)\n",new IntValue(16));
		testCase("fun x:bool => x end (true)", new BoolValue(true)); 
		testCase("fun x:int => fun y => x+y end (2) end (3)",new IntValue(5));// da erro 
		testCase("decl x = fun z:int => fun y:int => z*y end (5) end (5) in x end", new IntValue(25));
		testCase("decl x = fun z:int => fun y:bool => if y then z else 3 end end (true) end (5) in x end", new BoolValue(true));// da errro
		testCase("decl call = fun x:int => x + 1 end in call(5) end", new IntValue(6));
	}
	
	
}