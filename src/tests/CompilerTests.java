package tests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Test;

import main.Compiler;
import parser.ParseException;
import util.DuplicateIdentifierException;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.IntValue;

public class CompilerTests {

	// This test function was designed to work with Unix like systems.

	private void testCase(String expression, String result)
			throws IOException, InterruptedException, ParseException, FileNotFoundException,
			DuplicateIdentifierException, UndeclaredIdentifierException, ExecutionErrorException, TypeErrorException {

		Process p;

		p = Runtime.getRuntime().exec(new String[] { "sh", "-c", "rm *.j *.class" });
		p.waitFor();

		System.out.println("Compiling to Jasmin source code");

		Compiler.compile(expression);

		System.out.println("Compiling to Jasmin bytecode");

		p = Runtime.getRuntime().exec(new String[] { "sh", "-c", "java -jar jasmin.jar *.j" });
		p.waitFor();
		assertTrue("Compiled to Jasmin bytecode", p.exitValue() == 0);

		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

		StringBuffer output = new StringBuffer();
		String line = "";
		while ((line = reader.readLine()) != null) {
			output.append(line + "\n");
		}
		System.out.println(output.toString());

		p = Runtime.getRuntime().exec(new String[] { "sh", "-c", "java Demo" });
		p.waitFor();

		reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

		output = new StringBuffer();
		line = "";
		while ((line = reader.readLine()) != null) {
			output.append(line + "\n");
		}
		System.out.println("Output: #" + output.toString() + "#");

		assertTrue(result.equals(output.toString()));
	}

	private void testCase(String expression, int value)
			throws FileNotFoundException, IOException, InterruptedException, ParseException,
			DuplicateIdentifierException, UndeclaredIdentifierException, ExecutionErrorException, TypeErrorException {
		testCase(expression, value + "\n");
	}

	@Test
	public void BasicTest() throws IOException, InterruptedException, ParseException, DuplicateIdentifierException,
			UndeclaredIdentifierException, ExecutionErrorException, TypeErrorException {
		testCase("1\n", "1\n");
	}

	@Test
	public void testsLabClass02() throws Exception {
		testCase("1+2\n", 3);
		testCase("1-2-3\n", -4);
		testCase("4*2\n", 8);
		testCase("4/2/2\n", 1);
	}

	@Test
	public void RaidbossTest() throws Exception {
		testCase("decl x = decl y = 3 in 10*y end in x + decl y = 2 in x+y end end\n", "62");
		testCase("decl x = 1 in decl x = 2 in x*5 end + x end\n", "11");
		testCase("decl x = 1 y = 2 in decl x = 3 z = 4 in x + y + z end end\n", "9");
	}
}
