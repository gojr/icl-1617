package types;

public class RefType implements Itype {

	public final static RefType TYPE = new RefType();
	Itype type;

	private RefType() {
	}

	public RefType(Itype type) {
		this.type = type;
	}

	public Itype getType() {
		return type;
	}

	public void setType(Itype type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "ref(" + type.toString() + ")";
	}

}
