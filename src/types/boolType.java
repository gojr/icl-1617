package types;

public class boolType implements Itype {
	boolean type;

	public final static boolType TYPE = new boolType();

	private boolType() {
	}

	public boolType(boolean type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "boolean";
	}

}
