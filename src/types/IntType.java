package types;

public class IntType implements Itype{
int type;
//
//public IntType(int type){
//	this.type=type;
//}
//public int getType(){
//	return type;
//}
	
public final static IntType TYPE = new IntType();
	
	private IntType() {
		
	}
	public IntType(int type){
	this.type=type;
	}

	@Override
	public String toString() {
	return "int";
	}

	
}
