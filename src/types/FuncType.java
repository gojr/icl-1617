package types;

public class FuncType implements Itype {
	Itype iniType, retType;

	public FuncType(Itype iniType, Itype retType) {
		this.iniType = iniType;
		this.retType = retType;

	}

	public Itype getIniType() {
		return iniType;
	}

	public Itype getRetType() {
		return retType;
	}

	public String toString() {
		return iniType.toString() + "=>" + retType.toString();
	}

}
